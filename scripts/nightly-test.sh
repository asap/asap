#! /bin/bash

# Crontab job for testing ASAP and ASE on Niflheim (using Python 3). 
#
#Put this in crontab:
#    17 03 * * * $HOME/development/asap/scripts/nightly-test.sh $HOME/development/nightly-test/niflheim7-python3 intel
#
# Remember to update the path to the script, and the path to the folder
# where the tests are run.  That folder must contain cloned repos of ase 
# and asap.

if [[ $# -ne 3 ]]; then
    echo "Requires exactly three arguments (running directory, foss/intel and fast/slow)."
    exit 1
fi

cd "$1"
if [[ ! (-d asap && -d ase) ]]; then
    echo "The running directory must contain ase and asap."
    exit 1
fi

TOOLCHAIN="$2"
if [[ "$TOOLCHAIN" == "foss" ]]; then
    SUBCHAIN="gfbf"
    SUBSUBCHAIN="GCC-12.3.0"
elif [[ "$TOOLCHAIN" == "intel" ]]; then
    SUBCHAIN="iimkl"
    SUBSUBCHAIN="intel-compilers-2023.1.0"
else
    echo "The second argument must be either 'foss' or 'intel'."
    exit 1
fi

MODE="$3"
if [[ "$MODE" != "fast" && "$MODE" != "slow" ]]; then
    echo "The third argument must be either 'fast' or 'slow'"
    exit 1
fi
if [[ "$MODE" == "slow" ]]; then
    MODEARG="--slow"
else
    MODEARG=""
fi

# Check for a lock file
LOCKFILE=$PWD/lock.pid
if [ -f $LOCKFILE ]; then
    echo "Lockfile exists, PID = `cat $LOCKFILE`"
    exit 2
fi
echo `hostname` $$ > $LOCKFILE

# Load dependencies
# . /etc/bashrc
if [[ -n "$CPU_ARCH" ]]; then
    if [[ "$CPU_ARCH" -eq "ivybridge" ]]; then
	export CPU_ARCH=sandybridge
    fi
    export FYS_PLATFORM=Nifl7_${CPU_ARCH}_${TOOLCHAIN}
else
    echo "FYS_PLATFORM not set."
    env
    exit 5
fi

#export EASYBUILD_PREFIX=$HOME/easybuild/$CPU_ARCH
#module use $EASYBUILD_PREFIX/modules/all
module purge
module load matplotlib/3.7.2-${SUBCHAIN}-2023a
module load openkim-models/20210811-${SUBSUBCHAIN}
module load ${TOOLCHAIN}/2023a

ARCH=`uname -m | sed -e 's/ /_/g'`
export PYTHONPATH=`pwd`/ase:`pwd`/asap/Python:`pwd`/asap/${FYS_PLATFORM}:$PYTHONPATH
export PATH=`pwd`/asap/${FYS_PLATFORM}:$PATH

cd ase
git pull -q > /dev/null
if [[ $? -ne 0 ]]; then
    echo "git pull ase FAILED."
    rm $LOCKFILE
    exit 10
fi
find . -name '*.pyc' -delete > /dev/null 2>&1

cd ../asap
git pull -q > /dev/null
if [[ $? -ne 0 ]]; then
    echo "git pull asap FAILED."
    rm $LOCKFILE
    exit 11
fi

if [[ "$MODE" == "slow" ]]; then
    git clean -fdxq > /dev/null
    if [[ $? -ne 0 ]]; then
	echo "git clean -fdxq FAILED."
	rm $LOCKFILE
	exit 115
    fi
fi

make depend-maybe > ../makedepend.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "make depend-maybe FAILED."
    echo ""
    cat ../makedepend.log
    rm $LOCKFILE
    exit 12
fi

make all > ../makeall.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "make all FAILED."
    echo ""
    cat ../makeall.log
    rm $LOCKFILE
    exit 13
fi

cd Test
python TestAll.py $MODEARG > ../../testserial-devel.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Serial tests FAILED with developer ase."
    echo ""
    cat ../../testserial-devel.log
    rm $LOCKFILE
    exit 14
fi

mpirun -np 2 python TestAll.py --parallel $MODEARG > ../../testparallel-devel.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Parallel tests FAILED with developer ase."
    echo ""
    cat ../../testparallel-devel.log
    rm $LOCKFILE
    exit 15
fi

# Now run with system-wide ASE
module load ASE/3.24.0-${SUBCHAIN}-2023a

python TestAll.py $MODEARG > ../../testserial-inst_ase.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Serial tests FAILED with installed ase."
    echo ""
    cat ../../testserial-inst_ase.log
    rm $LOCKFILE
    exit 16
fi

mpirun -np 2 python TestAll.py --parallel $MODEARG > ../../testparallel-inst_ase.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Parallel tests FAILED with install ase."
    echo ""
    cat ../../testparallel-inst_ase.log
    rm $LOCKFILE
    exit 17
fi

rm $LOCKFILE
