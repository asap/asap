"""Export the files needed by the KIM api.

Usage:
  python export_to_kim.py folder

If the folder does not exist, it is created.
Two subfolders are created, models and model_drivers.
The ASAP models and model drivers are then copied into them.
"""


from __future__ import print_function

import os
import glob
import shutil
import string
from copy import copy
import subprocess
import sys

# The current version of the KIM API, to be written into .edn files.

kim_api_version = '2.0.0-beta.3'

# List all models to be exported, and the files that are specific to that model.
#
# For model drivers and independent models, the keys are the models /
# model drivers and the values the list of files.  For models that
# depend on a model driver, the keys are the models and the values are
# the model drivers.  It is assumed that each dependent model contains
# one or more parameter files named *.params; they are not listed.

emtdrivername = 'EMT_Asap__MD_128315414717_004'

model_drivers = {
    emtdrivername: ['EMT.cpp', 'EMTDefaultParameterProvider.cpp', 'EMTParameterProvider.h']
    }

independent_models = {}  # Same structure as model_driver_files

dependent_models = {
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_AlAgAuCuNiPdPt__MO_115316750986_001': emtdrivername,
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Ag__MO_303974873468_001': emtdrivername,
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Al__MO_623376124862_001': emtdrivername,
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Au__MO_017524376569_001': emtdrivername,
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Cu__MO_396616545191_001': emtdrivername,
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Ni__MO_108408461881_001': emtdrivername,
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Pd__MO_066802556726_001': emtdrivername,
    'EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Pt__MO_637493005914_001': emtdrivername,
    'EMT_Asap_MetalGlass_BaileySchiotzJacobsen_2004_CuMg__MO_228059236215_001': emtdrivername,
    'EMT_Asap_MetalGlass_PaduraruKenoufiBailey_2007_CuZr__MO_987541074959_001': emtdrivername,
    }

# List all files from the Basics and Potential directories.  If a .cpp file is given, the corresponding
# .h file does not need to be specified, it is added automatically.
basic_files = ['NeighborLocator.h', 'NeighborCellLocator.cpp', 'Asap.h',
               'AsapObject.cpp', 'Vec.cpp', 'TinyMatrix.h', 'Potential.cpp', 'SymTensor.h', 
               'Exception.cpp', 'IVec.h', 'Timing.cpp', 'mass.h', 'Debug.h',
               'Matrix3x3.cpp', 'TimingResults.h']

fake_headers = ['AsapPython.h', 'Atoms.h', 'Templates.h']

fake_header_template = """
// Fake header replacing an Asap header file.

#include "Kim%s"
"""

asapversion = None

def main():
    global asapversion
    asapversion = subprocess.check_output('python3 ../Python/asap3/version.py', shell=True).strip().decode()

    # Get the export folder from the command line
    if len(sys.argv) != 2:
        print("ERROR: Wrong number of arguments, expected 1, got {}.\n\n".format(len(sys.argv)-1))
        print(__doc__)
        sys.exit(-1)

    exportdir = sys.argv[1]
    kimdir = os.environ['KIM_HOME']
    
    if not os.path.isdir(exportdir):
        print("Creating folder '{}'".format(exportdir))
        os.mkdir(exportdir)
    kimmodeldir = os.path.join(exportdir, 'models')
    kimdriverdir = os.path.join(exportdir, 'model-drivers')
    for d in (kimmodeldir, kimdriverdir):
        if not os.path.isdir(d):
            print("Creating folder '{}'".format(d))
            os.mkdir(d)

    all_objects = list(model_drivers.keys()) + list(independent_models.keys()) + list(dependent_models.keys())
    print(all_objects)

    model_files = dict(model_drivers, **independent_models)  # Merge dictionaries

    # Add header files
    local_files = glob.glob('*.h') + glob.glob('*.cpp')
    for model in all_objects:
        isdriver = model in model_drivers
        dependent_model = model in dependent_models
        if dependent_model:
            my_basic_files = copy(basic_files)
        else:
            my_basic_files = basic_files + model_files[model]
        hdr = []
        for f in my_basic_files:
            if f.endswith('.cpp'):
                hdr.append(f[:-4]+'.h')
        my_basic_files.extend(hdr)
        if isdriver:
            model_dir = os.path.join(kimdriverdir, model)
        else:
            model_dir = os.path.join(kimmodeldir, model)
        print("Populating", model_dir)
        if not os.path.exists(model_dir):
            os.mkdir(model_dir)
        readmetxt = populate(model_dir, model, os.listdir(model))
        assert readmetxt is not None
        if not dependent_model:   # True for all current models and drivers!
            populate(model_dir, ['../Basics', '../Potentials'], my_basic_files)
            populate(model_dir, '.', local_files)
            for fake in fake_headers:
                fn = os.path.join(model_dir, fake)
                if not os.path.exists(fn):
                    print("Creating", fn)
                    f = open(fn, "w")
                    f.write(fake_header_template % (fake,))
                    f.close()

        cmakefilename = os.path.join(model_dir, 'CMakeLists.txt')
        shutil.copy('CMakeListsTemplate.txt', cmakefilename)
        with open(cmakefilename, "at") as cmakefile:
            if isdriver:
                cmakefile.write('set(MODEL_DRIVER_NAME "{}")\n'.format(model))
                cmakefile.write(modeldriverblurb)
            elif dependent_model:
                cmakefile.write('add_kim_api_model_library(\n')
                cmakefile.write('  NAME            "{}"\n'.format(model))
                cmakefile.write('  DRIVER_NAME     "{}"\n'.format(dependent_models[model]))
                paramfiles = [x for x in os.listdir(model) if x.endswith('.params')]
                cmakefile.write('  PARAMETER_FILES "{}"\n'.format(' '.join(paramfiles)))
                cmakefile.write('  )\n\n')
            else:
                raise RuntimeError("CMake specification for independent models not implemented")

            # Now list the source files for model drivers and independent models
            if isdriver or not dependent_model:                
                allfiles = os.listdir(model_dir)
                for f in sorted(allfiles):
                    if f.endswith('.cpp'):
                        cmakefile.write('  {}\n'.format(f))
                cmakefile.write(')\n')

        # Now make the kimspec.edn file
        # The Python .edn library writes a file that humans cannot read (no line breaks).
        # So it is done manually.
        readmetxt = readmetxt.translate(str.maketrans({'\n': r'\n', '"': r'\"'}))
        with open(os.path.join(model, 'kimspec.edn'), 'rt') as ednsource:
            metadata = ednsource.read().strip()
        assert metadata[-1] == '}'
        metadata = metadata[:-1]
        with open(os.path.join(model_dir, 'kimspec.edn'), 'wt') as edn:
            edn.write(metadata + '\n')
            edn.write(' "description" "{}"\n'.format(readmetxt))
            edn.write(' "kim-api-version" "{}"\n'.format(kim_api_version))
            if dependent_model:
                edn.write(' "model-driver" "{}"\n'.format(dependent_models[model]))
            edn.write(' "content-origin" "https://gitlab.com/asap/asap"\n')
            edn.write('}\n')


def populate(targetdir, sourcedirs, files):
    "Populate a model or model driver folder.  Returns the contents of the README file."
    if isinstance(sourcedirs, str):
        sourcedirs = [sourcedirs]
    readmetxt = None
    for f in files:
        if f == '.svn' or f == '.git':
            continue
        if f == 'kimspec.edn':
            continue   # This file is regenerated, not copied.
        for sourcedir in sourcedirs:
            sourcefile = os.path.join(sourcedir, f)
            if os.path.exists(sourcefile):
                break
        else:
            raise RuntimeError("File {} not found in folders {}.".format(f, str(sourcedirs)))
        targetfile = os.path.join(targetdir, f)
        if f == 'README':
            # README file is a template that should always be updated
            readme = string.Template(open(sourcefile).read())
            readmetxt = readme.substitute(version=asapversion)
            with open(targetfile, 'w') as ff:
                ff.write(readmetxt)
            print("Updated (and substituted)", targetfile)        
        else:
            if not os.path.exists(targetfile):
                newer = True
            else:
                newer = (os.stat(sourcefile).st_mtime > os.stat(targetfile).st_mtime)
            if newer:
                shutil.copy(os.path.join(sourcedir, f), targetfile) 
                print("Updated", targetfile)        
    return readmetxt

modeldriverblurb = '''
add_kim_api_model_driver_library(
  NAME                    ${MODEL_DRIVER_NAME}
  CREATE_ROUTINE_NAME     "model_driver_create"
  CREATE_ROUTINE_LANGUAGE "cpp"
  )

target_sources(${MODEL_DRIVER_NAME} PRIVATE
'''


                
if __name__ == '__main__':
    main()

            
            
