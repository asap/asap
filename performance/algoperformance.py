
import argparse
import platform
import subprocess
import re
import os
import multiprocessing
import time
import json
import numpy as np
from asap3.md.velocitydistribution import MaxwellBoltzmannDistribution, Stationary
from asap3.md.verlet import VelocityVerlet
from ase.md.verlet import VelocityVerlet as ASE_VelocityVerlet
from asap3.md.langevin import Langevin
from ase.md.langevin import Langevin as ASE_Langevin
from asap3.md.nvtberendsen import NVTBerendsen
from asap3.md.nptberendsen import NPTBerendsen, Inhomogeneous_NPTBerendsen
from asap3.md.bussi import Bussi
from asap3.md.nose_hoover_chain import NoseHooverChainNVT, IsotropicMTKNPT
from asap3.md.npt import NPT
from asap3.md import MDLogger
from asap3 import EMT
from asap3 import AsapThreads
try:
    from asap3 import MakeParallelAtoms
except ImportError:
    pass  # Will not support MPI if not compiled into code.
from ase.lattice.cubic import FaceCenteredCubic
from ase import units
from ase.parallel import world, parprint, paropen

def main():
    T0 = 300
    p0 = 0
    dt = 5 * units.fs
    tau = 500 * units.fs
    n_equi = 1500
    n_run = 500
    comp = 1 / (140 * units.GPa)

    parprint(f'Running on {world.size} MPI tasks.')

    atoms = make_system("Cu", 25, 25, 40, T=T0)
    equilibrate(atoms, T=T0, p=p0, timestep=dt, tau=tau, comp=comp, n=n_equi)

    # Now run all the different dynamics
    # A copy is made of the system, then it is run and the performance number
    # is returned
    results = {}

    parprint('Timing VelocityVerlet')
    perf = runperf(atoms, makeverlet, dt, n_run)
    results['VelocityVerlet'] = perf

    if world.size == 1:
        parprint('Timing ASE VelocityVerlet')
        perf = runperf(atoms, makeaseverlet, dt, n_run)
        results['VelocityVerlet (ASE)'] = perf

    parprint('Timing Langevin')
    perf = runperf(atoms, makelangevin, dt, n_run, temp=T0, tau=tau)
    results['Langevin'] = perf

    if world.size == 1:
        parprint('Timing ASE Langevin')
        perf = runperf(atoms, makeaselangevin, dt, n_run, temp=T0, tau=tau)
        results['Langevin (ASE)'] = perf

    parprint('Timing Bussi')
    perf = runperf(atoms, makebussi, dt, n_run, temp=T0, tau=tau)
    results['Bussi'] = perf

    parprint('Timing NoseHooverChainNVT')
    perf = runperf(atoms, makenhchain, dt, n_run, temp=T0, tau=tau)
    results['NoseHooverChainNVT'] = perf

    parprint('Timing NVTBerendsen')
    perf = runperf(atoms, makenvtberendsen, dt, n_run, temp=T0, tau=tau)
    results['NVTBerendsen'] = perf

    parprint('Timing NPTBerendsen')
    perf = runperf(atoms, makenptberendsen, dt, n_run, temp=T0, p=p0, tau=tau, comp=comp)
    results['NPTBerendsen'] = perf

    parprint('Timing Inhomogeneous_NPTBerendsen')
    perf = runperf(atoms, makeihnptberendsen, dt, n_run, temp=T0, p=p0, tau=tau, comp=comp)
    results['Inhomogeneous_NPTBerendsen'] = perf

    parprint('Timing IsotropicMTK')
    perf = runperf(atoms, makeisotropicmtk, dt, n_run, temp=T0, p=p0, tau=tau)
    results['IsotropicMTK'] = perf

    parprint('Timing NPT')
    perf = runperf(atoms, makenpt, dt, n_run, temp=T0, p=p0, tau=tau, comp=comp)
    results['NPT'] = perf

    for k, v in results.items():
        parprint(f'{k}   {v:.3f}')


def make_system(element, nx, ny, nz, T):
    # Make 10^5 atoms, i.e. 25000 FCC unit cells
    unitcells = 25000 * world.size
    size = bestfactors(unitcells)
    parprint('Making system of size', size)
    if world.rank == 0:
        atoms = FaceCenteredCubic(size=tuple(size), symbol=element)
    else:
        atoms = None
    if world.size > 1:
        cpus = bestfactors(world.size)
        parprint('CPU layout:', cpus)
        atoms = MakeParallelAtoms(atoms, cpus)
    return atoms

def equilibrate(atoms, T, p, timestep, tau, comp, n):
    atoms.calc = EMT()
    MaxwellBoltzmannDistribution(atoms, temperature_K=T)
    Stationary(atoms)
    dyn = NPTBerendsen(atoms, timestep, temperature_K=T, pressure_au=p, 
                       taut=tau, taup=tau, compressibility_au=comp)
    dyn.attach(MDLogger(dyn, atoms, '-', header=True, stress=True,
               peratom=True), interval=100)
    dyn.run(n)
    atoms.calc = None

def runperf(startatoms, dynmaker, timestep, nstep, temp=None, p=None, 
            tau=None, comp=None):
    atoms = startatoms.copy()
    atoms.calc = EMT()
    dyn = dynmaker(atoms, timestep, temp=temp, p=p, tau=tau, comp=comp)
    dyn.attach(MDLogger(dyn, atoms, '-', header=True, stress=True,
               peratom=True), interval=100)
    walltime = time.perf_counter()
    dyn.run(nstep)
    walltime = time.perf_counter() - walltime
    steptime = 1e6 * walltime / (nstep * atoms.get_global_number_of_atoms())
    steptime = world.sum(steptime)
    return steptime

def makeverlet(atoms, timestep, **kwargs):
    return VelocityVerlet(atoms, timestep)

def makeaseverlet(atoms, timestep, **kwargs):
    return ASE_VelocityVerlet(atoms, timestep)

def makelangevin(atoms, timestep, temp, tau, **kwargs):
    return Langevin(atoms, timestep, friction=1/(2*tau), temperature_K=temp)

def makeaselangevin(atoms, timestep, temp, tau, **kwargs):
    return ASE_Langevin(atoms, timestep, friction=1/(2*tau), temperature_K=temp)

def makebussi(atoms, timestep, temp, tau, **kwargs):
    return Bussi(atoms, timestep, temperature_K=temp, taut=tau)

def makenhchain(atoms, timestep, temp, tau, **kwargs):
    return NoseHooverChainNVT(atoms, timestep, temperature_K=temp, tdamp=tau)

def makenvtberendsen(atoms, timestep, temp, tau, **kwargs):
    return NVTBerendsen(atoms, timestep, temperature_K=temp, taut=tau)

def makenptberendsen(atoms, timestep, temp, p, tau, comp):
    return NPTBerendsen(atoms, timestep, temperature_K=temp, taut=tau, taup=tau,
                        pressure_au=p, compressibility_au=comp)

def makeihnptberendsen(atoms, timestep, temp, p, tau, comp):
    return Inhomogeneous_NPTBerendsen(atoms, timestep, temperature_K=temp, 
                                      taut=tau, taup=tau, pressure_au=p,
                                      compressibility_au=comp)

def makeisotropicmtk(atoms, timestep, temp, p, tau, **kwargs):
    return IsotropicMTKNPT(atoms, timestep, temperature_K=temp, 
                           tdamp=tau, pdamp=tau, pressure_au=p)

def makenpt(atoms, timestep, temp, p, tau, comp):
    pfact = tau**2 / comp
    return NPT(atoms, timestep, temperature_K=temp, ttime=tau, 
               externalstress=p, pfactor=pfact)



def bestfactors(n):
    pf = primefactors(n)
    pf.sort()
    pf.reverse()
    factors = np.ones(3, int)
    for f in pf:
        a = np.argmin(factors)
        factors[a] *= f
    return factors

def primefactors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

if __name__ == '__main__':
    main()
    
