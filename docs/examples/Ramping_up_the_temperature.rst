.. _Ramping up the temperature:

==========================
Ramping up the temperature
==========================

In this example, a small cubic nanoparticle is melted by increasing
the temperature stepwise.  Five plots are generated at each
temperature using PrimiPlotter.

:git:`~docs/examples/Melting.py`

.. literalinclude:: Melting.py
