.. _Running on parallel computers:

=============================
Running on parallel computers
=============================

Asap can run on computers with multiple CPU cores, and on clusters of
such computers.  This script runs molecular dynamics on the system
created in the :ref:`previous example <Creating a dislocation in a slab>`.

You have to be aware of a number of details in the script, where
special care is used to make it work in parallel.

* The ``Trajectory`` object and the MD dynamics (``Langevin``) are
  imported from ``asap3`` instead of from ``ase``.  You get special
  versions that can handle the distributed atoms.

* Output written to a file should only be written by the master node.
  That is done by using ``ase.parallel.paropen`` to open the file.

* The input file is not read the usual ASE way, which would return all
  the atoms in all processes.  The special ``Trajectory.get_atoms``
  method is used instead, returning a distributed, Asap-specific Atoms
  object.

* The total number of atoms (used to calculate energy per atom) is
  returned using ``atoms.get_number_of_atoms()``.  The usual
  ``len(atoms)`` would instead return the number of atoms *on this
  processor*.

* The PTMobserver also works in parallel, but it is necessary to
  specify a cutoff that is shorter than the cutoff of the Calculator
  used in the dynamics, since the Calculator cutoff determines how
  much information each processor stores about atoms managed by other
  processors.  The same goes for CNA.

* Plotting is done using a special ``ParallelPrimiPlotter``.  It is
  identical to ``PrimiPlotter`` except that it works in parallel.  For
  historical reasons it is in ``ase.visualize``, it will probably be
  moved to ``asap3.visualize`` in the future.

:git:`~docs/examples/Parallel.py`

.. literalinclude:: Parallel.py

