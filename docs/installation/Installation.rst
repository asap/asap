.. _Installation:

    
============
Installation
============

.. contents::

Asap is released under the GNU Lesser Public License version 3. See the file
LICENSE which accompanies the downloaded files.  For more information,
see our :ref:`License <License>` page.

**NIFLHEIM USERS:** Please see the page :ref:`Niflheim`.


Simple installation
===================

In the vast majority of cases, you can simply install Asap using pip.
You will need to have numpy installed (including header files,
sometimes that package is called numpy-dev).  You need a C++ compiler installed (probably g++). 
If you want to run Asap
in parallel, you should also have openmpi (or another MPI
implementation) installed.  In most scientific Linux environments, these prerequisites are already fulfilled.

You can install Asap with the
command

::

  pip install --upgrade --user asap3

Note that we pass the ``--user`` option to cause installation into
``$HOME/.local`` instead of into the global Python distribution.  We
explicitly tell pip to upgrade if an older installation is present.

If you install into a `virtual environment`_ (a very good idea!) you should leave
out the ``--user`` option.

.. _`virtual environment`: https://docs.python.org/3/library/venv.html


Testing the installation
========================

Of course it works.  But it may still be wise to test it!

Unfortunately, when pip-installing Asap the test suite is not installed (yet?).
So testing requires downloading the source code.

* Get the source code from `GitLab's asap release page`_.

* Unpack it into a temporary folder

* Go to the ``Test`` subfolder, and run the test suite

  - run ``python TestAll.py`` for the serial test suite.  Or run ``python TestAll.py --slow``
    if you want a more thorough test, it may take 5-10 minutes.

  - run ``./TestParallel.sh`` for the parallel test suite (only if you have
    MPI installed on your machine).

* Once the test has passed, you may delete the temporary folder with the source code.


Optimized installation
======================

Building with the Intel Compiler for optimal performance
--------------------------------------------------------

Compiling with the Intel Compiler (icc/icpc) gives more than a factor two in
performance compared to using the GNU Compilers (gcc/g++), used per
default when installing with pip.  If you have the Intel Compiler
available, you can use it like this:

::

  pip install --upgrade --user --install-option="--with-intel" asap3


NB: The Intel compilers are a commercial product, but it is available
for free for students for *personal* use.


Manual installation from source
===============================

If you want to tweak the installation, if you need the developer
version, or if the simple installation does not work for some reason
you can install manually´.


Download Asap
-------------

Download the source code of the latest release from `GitLab's asap release page`_.

.. _`GitLab's asap release page`: https://gitlab.com/asap/asap/-/releases


... or get the developer version from GitLab
--------------------------------------------

It is also possible to get the latest developer version of Asap from GitLab::

  $ git clone https://gitlab.com/asap/asap.git

Then proceed to compile it using the Installation instructions, in
particular the section regarding `In-place installation for Git
users`_.

If you want to browse the source code (including changes), it is most
convenient to use the `GitLab source browser`_

.. _GitLab source browser: https://gitlab.com/asap/asap/tree/master


Quick installing guide
----------------------

1. If you have not already done so, you should `install the
   the Atomic Simulation Environment`__.

2. Unpack the tarball and rename the resulting folder to something sensible::

     tar xvf asap-ASAP_VERSION_3_10_0-a78278070c997b302c8117d8174c8914d5f3d273.tar.gz
     mv asap-ASAP_VERSION_3_10_0-a78278070c997b302c8117d8174c8914d5f3d273 asap-3.10.0
     cd asap-3.10.0

3. Build with the standard command for building Python packages::

     python setup.py build


You now have to put ``$HOME/asap-3.10.0/build/scripts-2.7`` on your
PATH and ``$HOME/asap-3.10.0/build/lib.linux-x86_64-2.7`` on your
PYTHONPATH (the exact folder names will depend both on the Python
distribution you use).


__ https://wiki.fysik.dtu.dk/ase/install.html


OpenKIM support
===============

Starting with version 3.11.7, support for OpenKIM v. 2.X is
automatically enabled when compiling Asap. The OpenKIM libraries are
found using the ``pkg-config`` program, as found in the ``pkgconf``
package (or its now obsolete predecessor ``pkg-config``).

Starting with version 3.13.2, it is no longer possible to install
OpenKIM without ``pkg-config`` , as the alternate way rarely worked
anyway. 



Special installation instructions
=================================

Installing on Niflheim
----------------------

Please see the page :ref:`Niflheim`.


Installation using the Intel compiler and/or installation on clusters
---------------------------------------------------------------------

If you want to install 
Asap with the Intel C++ compiler (for better performance), you can
install Asap using GNU make.  The Intel compiler is
automatically detected and used if available.

To install the serial version::

  make depend
  make serial

To install both the serial and parallel version::

  make depend
  make all

You need to put the absolute paths to the folders ``Python`` and ``x86_64`` 
on your ``PYTHONPATH``, so Python can find Asap.  Note that the folder named 
``x86_64`` will have a different name if you do not have an Intel CPU or if
you have an unusual Unix distribution.  ``make info`` will tell you the name
(look for the variable ``OBJDIR``).

For more information, please read the :ref:`Installing Asap with a Makefile`.


In-place installation for Git users
-----------------------------------

If you got Asap by cloning the GitLab repository, and always want to use the newest version,
you probably want to compile it in-place.

**IMPORTANT:** If you use the developer version of Asap from GitLab,
you should also use the developer version of ASE from GitLab.

**For Niflheim users:**

See the page :ref:`Niflheim`.


**For everybody else:** 

Compile Asap with::

  make depend
  make serial

if you only want the serial version, or::

  make depend
  make all

if you also need the parallel version.  After updating from gitlab, you
only need to re-run ``make depend`` if ``make serial`` (or ``make
all``) fails with an instruction to do so.

To use Asap compiled in this way, you need to put it on your PATH and your
PYTHONPATH.  Assuming that Asap is placed in $HOME/asap, these
variables should be set to::

  export PATH=$HOME/asap/XXX:$PATH
  export PYTHONPATH=$HOME/asap/Python:$HOME/asap/XXX:$PYTHONPATH

where XXX is the output of the command ``uname -m``.  


Installation on a Mac
---------------------

Please read :ref:`Installing ASE Asap and GPAW on a Mac`


.. toctree::
   :hidden:

   Installing_ASE_Asap_and_GPAW_on_a_Mac
   Installing_Asap_with_a_Makefile
