.. _asap:

==========================
ASAP - As Soon As Possible
==========================

.. image:: https://badge.fury.io/py/asap3.svg
    :target: https://pypi.org/project/asap3/


ASAP is a calculator for doing large-scale classical molecular
dynamics within the `Atomic Simulation Environment`_ (ASE).

.. _`Atomic Simulation Environment`: https://wiki.fysik.dtu.dk:/ase

Asap supports Python 3.9 or later.  Asap can be installed with ``pip install asap3``.

ASAP currently implements:

* The Effective Medium Theory (:ref:`EMT`) potential for the elements Ni,
  Cu, Pd, Ag, Pt and Au (and their alloys).  There is also
  experimental support for CuMg and CuZr alloys.

* Support for all models published by the OpenKIM.org_ project.
  Currently, more than 150 potentials are available this way.  See the
  page :ref:`OpenKIM support`.

* A number of experimental or in-development potentials.

ASAP supports simulations on parallel clusters.  Simulations with
hundreds of CPU cores are possible with little overhead. 

:ref:`License <License>`: Asap is released under the GNU Lesser General Public License (LGPL).  Older versions were released under the GPL.  See the page :ref:`License` for details.

.. _OpenKIM.org: https://openkim.org

Getting started
---------------

* Installing Asap:  See the :ref:`Installation` page.

* Using Asap:  Start with the :ref:`Examples`.  There is also a :ref:`Manual`.


Information for users at DTU
----------------------------

* How to run :ref:`Niflheim`.

The name
--------

ASAP stands for As Soon As Possible, reflecting that simulations with
Asap should be fast and flexible, so you get the result as soon as
possible.  New features should be implemented as soon as possible, but
not before they are needed.

An alternate interpretation of ASAP is "Accelerated Simulations and
Potentials (for the Atomic Simulation Environment)".


News (see also the :ref:`Change log`)
-------------------------------------

* Version 3.13.6 released 31. January 2025.  Support two new NVT dynamics
  algorithms (Bussi and NoseHooverChainNVT) and one new NpT dynamics 
  (IsotropicMTK).  Bussi requires ASE 3.24.0, the two other require 
  yet-to-be-released ASE 23.25.0.

* Version 3.13.5 released 11. October 2024.  Support numpy version
  2.X (1.X is still working).

* Version 3.13.4 released 28. February 2024.  Fix installation issue
  and compatibility with ASE master branch.

* Version 3.13.3 released 30. January 2024.  Fixes bad compiler
  optimization with Intel OpenAPI compilers (would otherwise crash).
  
* Version 3.13.2 released 20. October 2023.  Fix broken OpenKIM
  installation in version 3.12.1.  Allow more than 2 billion atoms in
  parallel simulations.

* Version 3.13.1 released 20. June 2023.  More robust installation.
  Docs in Sphinx / readthedocs.io.

Older news: See the :ref:`Change log`.

.. _Asap on Niflheim: Niflheim


.. toctree::
   :hidden:

   installation/Installation
   examples/Examples
   manual/Manual
   platforms/niflheim
   Performance
   development/Development
   License   
   
