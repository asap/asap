import datetime
import sys

import sphinx_rtd_theme

assert sys.version_info >= (3, 6)

sys.path.append('.')

extensions = [#'images',
              'ext',
              'sphinx.ext.autodoc',
              'sphinx.ext.doctest',
              'sphinx.ext.extlinks',
              'sphinx.ext.viewcode',
              'sphinx.ext.napoleon',
              'sphinx.ext.intersphinx']

# Read ASAP version.
vvars = {}
with open('../Python/asap3/version.py', 'r') as vfile:
    exec(vfile.read(), vvars)

extlinks = {
    'doi': ('https://doi.org/%s', 'doi: %s'),
    'arxiv': ('https://arxiv.org/abs/%s', 'arXiv: %s'),
    'mr': ('https://gitlab.com/asap/asap/-/merge_requests/%s', 'MR: !%s'),
    'issue': ('https://gitlab.com/asap/asap/-/issues/%s', 'issue: #%s'),
}

templates_path = ['templates']
source_suffix = '.rst'
master_doc = 'index'
project = 'ASAP'
copyright = f'{datetime.date.today().year}, Jakob Schiøtz, DTU Physics'
release = vvars['__version__']
exclude_patterns = ['build']
default_role = 'math'
pygments_style = 'sphinx'
autoclass_content = 'both'
modindex_common_prefix = ['asap3.']
intersphinx_mapping = {
    'python': ('https://docs.python.org/3.10', None),
    'ase': ('https://wiki.fysik.dtu.dk/ase', None),
    'numpy': ('https://numpy.org/doc/stable', None),
    'cupy': ('https://docs.cupy.dev/en/stable', None),
    'scipy': ('https://docs.scipy.org/doc/scipy', None),
    'pytest': ('https://docs.pytest.org/en/stable', None),
    'mayavi': ('http://docs.enthought.com/mayavi/mayavi', None)}


html_theme = 'sphinx_rtd_theme'
html_style = 'css/theme.css'
html_title = 'ASAP3'
html_favicon = '_static/favicon.ico'
html_logo = "_static/altlogo.png"
html_static_path = ['_static']
html_last_updated_fmt = '%a, %d %b %Y %H:%M:%S'

autodoc_typehints = 'description'
autodoc_typehints_description_target = 'documented'

