.. _Change log:

==========
Change log
==========

*Keeping a formal change log is new - started in version 3.10.*

Next version
=============

*None yet*

Version 3.13.6
==============

Major changes:

* Add IstotropicMTK dynamics.
* Add Bussi dynamics.
* Add NoseHooverChainNVT dynamics.

Minor/internal changes:

* Fix compatibility with ASE 3.24.0. (Import of filters; Bug fix in BundleTrajectory).
* Many new tests of NVT and NpT dynamics algorithms.

Version 3.13.5
===============

* Remove leftover dependency on the deprecated ``distutils`` package.
* Compatibility with numpy 2.X (the later 1.X still work).
* Compatibility with newest Python/setuptools (compilation of C++
  modules changed during summer 2024).

Version 3.13.4
===============

* Fix installation if numpy was not preinstalled (e.g. in pristine
  venv).
* Remove unused argument causing ASE Trajectory to crash (ASE master branch).

Version 3.13.3
===============

* Bug fix: Would crash with more than 4 billion atoms.
* Bug fix: Intel OneAPI compilers would optimize a dubious construct
  incorrectly (fortunately caught by assert statement).
* As ASE now requires Python 3.8 or newer, so does Asap.  Outdated
  constructions updated with ``pyupgrade``.
* Fix test OpenKIM_AllModels so it skips tests of potentials that are
  broken (or in a few cases incompatible with this test).


Version 3.13.2
===============

* Documentation updates.
* Bug fix: Version 3.13.1 did not compile with OpenKIM support when
  installed with pip.
* Allow for parallel simulations with more than 2\ :sup:`31` (2 billion)
  atoms, as long as there are less than 2\ :sup:`32` on any single core.

Minor changes:

* Silence compiler warnings, fix TAB/indentation in a few source files.

Version 3.13.1
===============

Version 3.12.1 released 20. June 2023. More robust installation. Docs in Sphinx / readthedocs.io.

Major changes:

* Update build system from distutils to setuptools.
* Moved documentation from MoinMoin wiki to sphinx (in the :git:`~docs`
  folder), hosted at https://asap3.readthedocs.io.
  
Minor changes:

* Tiny change to accommodate newer NumPy: ``numpy.float`` is deprecated.

  
  
Version 3.12.12
===============

* Fix compatibility with newest numpy (1.23.3).

Version 3.12.11
===============

* Bug fix: Fix the temperature reported by atoms in parallel simulations with constraints.
* Adapt Langevin to work with master branch of ASE (as well as latest release).
* Make the FixAtom constraint work in parallel simulations where atoms are deleted.


Version 3.12.8
==============

* Fix a compilation issue with some C++ compilers (incl. gcc 4.8.5).  Issue #46.

Version 3.12.7
==============

Version 3.12.7 released 21. April 2021.  Mainly bug fixes incl. a small memory leak in parallel simulations.

* Fix ``Atoms.get_moments_of_inertia()`` and ``Atoms.get_angular_momentum()`` for parallel simulations.
* Add ``ZeroRotation`` function to ``asap3.md.velocitydistribution`` as the one in ASE does not work for parallel simuations.
* Add ``Atoms.delete_atoms_globally`` to delete atoms in a parallel simulation.
* Fix ``NVTBerendsen`` and ``NPTBerendsen``, so they work in parallel.
* Fix that changing boundary conditions could cause a crash (issue #44).
* Fix that ``FixAtoms`` would crash if multiple dynamics were using the same atoms.
* Fix that running some analysis tools between changing the atoms and calculating energies/forces would cause the energy/force calculation to crash or give wrong results (parallel simulations only, issue #43).
* Temporarily disable storing FixAtoms constraints in trajectory files in parallel simulations (would crash).
* Fix a small memory leak in parallel simulations (introduced with Python 3.X support).


Version 3.12.3
==============

Version 3.12.3 released 22. March 2021.  Bug fix release: fixes a memory leak introduced in 3.12.1.  May be significant in some use cases.

* Fix a memory leak introduced in version 3.12.1.
* Account for removed degrees of freedom in the asap-optimized version of the FixAtoms constraint.


Version 3.12.2
==============

Version 3.12.2 released 2. February 2021.  Identical to 3.12.1, except
that the latter failed to build in some corner cases.

* Fix a corner case where compilation would crash (including for EasyBuild with GCC 10.X).
* Add an extra test of the LennardJones potential, confirming it worked as intended (but there was an error in the docs).


Version 3.12.1
==============

Version 3.12.1 released 29. January 2021.  Works with ASE 3.21.1.  No longer needs a special python executable for parallel simulations.

Major changes:

* **Dropped support for Python 2.7** as ASE no longer supports this ancient version.
* Fix handling of temperature in all modules.  Temperature should now be given in Kelvin
  using the named parameter ``temperature_K``.  The old way of specifying temperature is
  kept for backwards compatibility.
* Fix handling of pressure in constant-pressure dynamics, they now all use atomic units.
  (This point and the previous mirror the same changes in ASE).
* Parallel simulations no longer require a custom-built Python executable.  Just start
  ``python`` with mpiexec and import the ``asap3`` module.
* Now **requires ASE version 3.21.1 or later**.  

Minor changes:

* Fixes to the GitLab CI (they updated something).
* Better handling of CFLAGS when building in some environments (such as EasyBuild).
* Adaptations to ASE 3.18.1 and 3.19.0b1 

  - pbc removed from unit cell object.
  - The time step parameter of ``VelocityVerlet`` renamed to ``timestep`` from ``dt``. 
  - ``atoms.get_number_of_atoms()`` renamed to ``atoms.get_global_number_of_atoms()``.
  - ``atoms.get_scaled_positions()`` will stop wrapping to [0,1[ per default.
* Move calculation of dynamic part of stress from Asap to ASE (expected in ASE 3.19).  Note that Asap automatically will detect and old ASE and do the calculation itself.
* Move ``PrimiPlotter`` from ASE to Asap.
* ``get_neighbors_querypoint`` method added to FullNeighborList by Peter Jørgensen.  Thanks!
* Obsolete tests removed from test suite, and some more MD algorithms tested properly.
* Remove the (obsolete) PickleBackend from the BundleTrajectory.
* Update build process to work in virtual environments.

Version 3.11.10
===============

Version 3.11.10 released 15. August 2019.  Updated to ASE 3.18.0 and kim-api (a.k.a OpenKIM) 2.1.2.  **Last version supporting Python 2.7** (with ASE 3.17.0).

* Update to handle OpenKIM's kim-api 2.1.2 (only the test suite was affected).
* Handle that massively parallel simulations may result in cores with no atoms (hopefully only rarely, as this hurts performance).
* Allow test suite to run on four cores as well as two.
* Small adaptiation to ASE 3.18.0.

Version 3.11.9
==============

Version 3.11.9 released 5. April 2019.  Updated to use OpenKIM 2.0.2.

* Bug fix release: Version 3.11.8 did not always compile with OpenKIM.


Version 3.11.8
==============

* Use pkg-config to locate OpenKIM library, if available.
* Asap to recent change in ASE: Atoms.cell may be something more complicated than a numpy array.
* OpenKIM version 2.0.2 has been released, necessitating a minor change in the build process.


Version 3.11.6
==============

Version 3.11.6 released 8. February 2019.  Updated to the final (non-beta) release of OpenKIM 2.0.0.

* OpenKIM version 2.0.0 has officially been released, and a method name has changed since the last beta version.  Asap was updated accordingly.


Version 3.11.5
==============

* Fix a small memory leak that also affected the exported OpenKIM model (thanks to the OpenKIM.org team for detecting it and helping me find it).
* OpenKIM models had their version number bumped due to fixing the memory leak, and their metadata updated to reflect best practice in the OpenKIM project.


Version 3.11.4
==============

* KIM models exported from Asap to the OpenKIM project have had their files updated with the KIM ID, and a few corrections.

Since compiling this version produces exactly the same modules as version 3.11.3 (a part from the version number), **version 3.11.4 has not been released** on `PyPI <https://pypi.org/project/asap3/>`_ and will not be installed on Niflheim.

Version 3.11.3
==============

Version 3.11.3 released 21. December 2018.  Now uses :ref:`OpenKIM version 2 <OpenKIM support>`.

* **MAJOR CHANGE:** Asap now uses OpenKIM version 2.X.  Almost all version 1.X OpenKIM models have been ported.  The main consequence of this change is that parallel simulations with OpenKIM models now works consistently.
* Numerous internal changes: 

  * Stop redefining assert() as a macro, it caused portability problems.
  * Source tree slightly reorganized (new Potentials folder).
  * Catch all C++ exceptions by reference.
  * C++ potentials now calculate Virials which are converted to stresses in Python.


Version 3.10.10
===============

* Stop relying on the C++ 2011 standard, as surprisingly many compilers do not comply properly.  This allows building with Anaconda Python on a Mac, where the default compiler does not include the c++ 2011 header files correctly.
* Fix a few installation issues: Really old system Python interfering (Issue #27) and failure to build with debugging Python (Issue #26).
* Update PTM module to latest version from https://github.com/pmla/polyhedral-template-matching (commit 2ea9c80).
* Compatibility with upcoming ASE release (iterimages method on Atoms object).


Version 3.10.8
==============

Version 3.10.8 released 6. December 2017.  Mainly bug fixes, but also
submission script for clusters using SLURM (only tested on Niflheim).

* Prevent compilation with versions of the Intel compiler known to be bad.

* Bug fix: Reusing an EMT calculator with atoms containing different
  elements could lead to crashes or even wrong results.

* Updates to the asap-sbatch script for submitting to some clusters (and let pip install it).


Version 3.10.7
==============

* Fix an incompatibility with ASE 3.15.0.

* Fix some compilation issues with some newer compilers.

* Add ability to set a RNG seed in Langevin dynamics, for deterministic MD runs.  Thanks to Kasper P. Lauritzen for implementing this.

* Add analysis.findcluster and analysis.cutcluster modules (to be documented).

* Bug fix: PTM would sometimes crash (multiple bugs fixed).

* Bug fix: After a recent optimization, the dislocation setup module
  would lose the screw component of mixed dislocations 50% of the time.

* Bug fix: Langevin dynamics was totally broken when used with the
  FixAtoms constraints.

* Bug fix: The FixAtoms constraint was incorrectly written to
  Trajectory files, resulting in an unreadable file.  It still
  does not work in parallel simulations.
  

Version 3.10.6
==============

Version 3.10.6 released 29. March 2017.  Minor bug fixes.

* Bug fix: Optimized VelocityVerlet dynamics did not respect if
  the atoms had nonstandard masses.

* Updated test suite to reflect that the atomic masses in ASE have
  finally been updated to the latest IUPAC values (2016).

* Updated module for :ref:`Creating nanocrystalline materials`. 

* Fix an issue where the Asap installtion script would fail on some
  Linux distributions if MPI was not available.


Version 3.10.5
==============

Version 3.10.5 released 8. February 2017.  Minor bug fixes and
maintains compatibility with the newest ASE (version 3.13).

* Maintain compatibility with ASE version 3.13.

* Add BundleTrajectory.get_atoms_distributed message, see :ref:`Input and output`.

* Bug fix in PTMdislocation: classified atoms in stacking faults incorrectly.

* Return the ase.calculators.calculator.PropertyNotImplementedError if
  a propery is not defined by a calculator.  With old ASE versions,
  return its own such exception - both are derived from
  NotImplementedError.

* Fix error in test suite that appeared with the upcoming NumPy version 1.12.0.

* Give a reasonable error message if one or more dimensions of the
  unit cell are zero (as produced by newest developer version of ASE).

* OpenKIM now supported in Python 3.X.

* More portable BundleTrajectory files (independent of Python version)
  with ASE 3.14 (supporting the Ulm backend for BundleTrajectory).
  Per default, also saves 50% on disk space by storing data in single
  precision.


Version 3.10.3
==============

* Bug fix: OpenKIM support was not enabled correctly when installing
  with setup.py.
  

Version 3.10.2
==============

Versions 3.10.2 and 3.10.3 were released 17. and 24. November 2016.  Minor bug fixes and 
  more reliable installation. 

* A 25% reduction in memory footprint (to approx. 800 bytes/atom for EMT), at
  no cost in performance (around 1% gain, actually).

* Fix the dislocation setup module for Python 3.

* Experimental support for compiling the EMT potential with Intel Math
  Kernel Library and GCC, giving *almost* as good performance as using
  the Intel compilers.

* Re-enable support for compiling with the Intel compiler for better performance.

* More reliable installation on different Linux distributions.

* Fixed PTMdislocation tool, so it works in parallel (PTM itself
  already worked)

* Parallel binary compiled with optimization everywhere.


Older news
==========

Announcements from before a formal ChangeLog was kept.

* Version 3.10.0 was released 26. October 2016.  Support for Python 3,
  and *much* easier :ref:`Installation`.

* The 9. November 2015: Asap source code moved from SVN (our own server) to Git at GitLab: https://gitlab.com/asap/asap

* Version 3.8.4 was released 17. December 2014.  Fix installation
  issue with :ref:`OpenKIM support`.

* Version 3.8.3 was released 11. December 2014.  :ref:`OpenKIM support`.  Enjoy more than 150 new potentials without having to implement them first.

* Version 3.4.5 was released 29. May 2012, mainly to fix compatibility with the
  latest ASE.

* Version 3.4.0 was released 6. January 2011.  :ref:`License` changed to LGPL.

* Version 3.3.5 was released 12. August 2010.  It maintains
  compatibility with the newest version of ASE.

* Version 3.2.6 was released 7. December 2009.  It is a bug fix
  release, fixing a way too high memory consumption in parallel
  simulations with many CPUs, and an occasional crash in long,
  parallel simulations.  It also introduces `asap-qsub`_, a tool for
  easy job submission on Niflheim.

* Version 3.2.0 was released 20. August 2009.  **Asap version 2.X is now obsolete and unsupported!**

* The 20. August 2009: SVN repository reorganisation, in preparation for releasing version 3.2.

* Version 3.0.2 was released 15. October 2008.  This is the first
  version for ASE version 3.X.  

* Version 2.20.0 was released 11. July 2008.  Performance enhancements for Monte Carlo EMT simulations and for multithreaded MD simulations.

* Version 2.18 was released 12. March 2008, with two new potentials
  and a new parallelization paradigm.

* The Asap source code has moved from CVS to SVN and the bug tracking from Bugzilla to trac (November 2007).

* Information about :ref:`Performance` added 22. November 2006.

* Version 2.16.1 was released 27. October 2006.  Mainly some internal changes and
  a few minor bugfixes.  Now works on the Opteron CPUs (such as the Niflheim cluster).

* Version 2.14 was released 29. November 2005.  NPT dynamics now work in parallel
  simulations.  A memory leak relating to NPT dynamics was removed.

* Version 2.12 was released 15. November 2005.  It requires Campos ASE_ version 2.1.
  It contains a few minor bug fixes, and support for `generating general crystal structures`_.

* Version 2.10 was released 16. July 2005.  It provides compatibility
  with newer versions of SWIG (1.3.23 or newer).  If you get weird
  messages about overloaded functions when calculating the stress,
  update to this version.

* Version 2.8 was released 23. May 2005.  Mainly a bug fix release
  (including a nasty one, where the zero of the potential energy could
  be in different places).  New feature: the Subset filter can now be
  used in parallel simulations.

* Version 2.6.1 was released 20. April 2005.  The only change from
  version 2.6 is that the Distutils-based installation (``python
  setup.py install``) now works again.

* Version 2.6 has been released 15. April 2005.  A few extra modules
  have been added, mainly for analysis, and a few bugs have been
  fixed.  Asap no longer requires cblas for some analysis tools.

* Version 2.4 has been released in February 2005.  This is supposed
  to be a working release.  Anything not working now is a bug (except
  the Quasicontinuum method, which may or may not be reintroduced in
  a future version).

.. _generating general crystal structures: Setting_up_complicated_structures
.. _asap-qsub: Niflheim
.. _ASE: http://wiki.fysik.dtu.dk/ase
