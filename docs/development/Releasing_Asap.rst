.. _Releasing Asap:

==============
Releasing Asap
==============


#. Check or update the version number in Python/asap3/version.py 

#. Make sure everything is checked in in git.

#. Run the test suite, test both normal serial runs, threaded runs and
   parallel runs.

#. Update the :ref:`Change log`.

#. Make a tag on gitlab.com named ASAP_VERSION_3_X_Y.

#. Create a release from this tag.  This will make
   that version available for download as .zip and .tar.gz files.

#. Make the .tar.gz file that will be uploaded to PyPI::

     python setup.py sdist

#. Upload to PyPI::

     twine upload dist/asap3-3.X.Y.tar.gz 

#. Test installation e.g. in a virtual machine::

     pip install --upgrade asap3 --user

#. Update the download page of this wiki, pointing to the tar.gz file
   on gitlab.com.  Announce new version on the Wiki front page.

#. Build new packages for Niflheim.

#. Test that asap works and install on Google Colab by running
   ``Test/TestAsap_Colab.ipynb``.
   
