.. _Development:

=====================
Development resources
=====================


Asap stuff
==========

* `Bug tracking`_ (GitLab).
* `Source browsing`_ (GitLab).
* :ref:`Profiling` Asap.
* :ref:`Releasing Asap`.
* :ref:`Using AddressSanitizer`.
* :ref:`Running OpenKIM containers` for debugging.
* :ref:`Change log`.

.. _Source browsing: https://gitlab.com/asap/asap/tree/master
.. _Bug tracking: https://gitlab.com/asap/asap/issues
.. _trac bug list: https://trac.fysik.dtu.dk/projects/Asap/report/1
.. _buildbot for Asap: https://asap-buildbot.fysik.dtu.dk/waterfall


.. toctree::
    :hidden:

    Profiling
    Releasing_Asap
    Using_AddressSanitizer
    Running_OpenKIM_containers
    Change_log


Useful manuals 
==============

* `NumPy manual`_.
* `SciPy manual`_.
* `POSIX Threads programming`_.
* Linux tutorial: `POSIX threads`_.

.. _NumPy Manual: https://numpy.org/doc/stable/reference/
.. _SciPy Manual: https://docs.scipy.org/doc/scipy/reference/
.. _POSIX Threads programming: https://computing.llnl.gov/tutorials/pthreads/
.. _POSIX threads: http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html


Using git
=========

The Asap repository is on GitLab_.  If you develop Asap regularly, you
should have a developer version of both ASE and Asap.  Please see how
to set up GitLab_ and git in the ASE developer documentation: `GitLab repository`_

Once you have set up the ASE repository and your own clone at GitLab,
do the same for Asap:

* Go to https://gitlab.com/asap/asap and fork the project.  You will now
  have a fork situated at https://gitlab.com/your-user-name/asap

* Clone your fork to your own machine::

    $ git clone git@gitlab.com:your-user-name/asap.git
    $ cd asap

* Add and track ``upstream`` (the original asap repository) on your
  own machine::

    $ git remote add upstream git@gitlab.com:asap/asap

  You now track both your own clone and the main repository.

* If you want to update the PTM part of the source, you also need to
  track that repository.  The Polyhedral Template Matching module is
  living in its own repository (on github), you need to access it to
  update which version Asap uses.  *Most developers don't need
  this!*::

    git remote add ptm_remote https://github.com/pmla/polyhedral-template-matching.git

You now track these repositories::

  $ git remote -v
  origin	git@gitlab.com:schiotz/asap.git (fetch)
  origin	git@gitlab.com:schiotz/asap.git (push)
  upstream	git@gitlab.com:asap/asap (fetch)
  upstream	git@gitlab.com:asap/asap (push)
  ptm_remote	https://github.com/pmla/polyhedral-template-matching.git (fetch)
  ptm_remote	https://github.com/pmla/polyhedral-template-matching.git (push)

where the last two are optional.

Making changes
--------------

These instructions are brief.  For details, se the ASE page `Making changes`_.

* Make sure your own master branch is up-to-date with upstream, and
  does not contain local edits::

    $ git checkout master
    $ git fetch upstream
    $ git merge upstream/master --ff-only

* OPTIONAL: Keep your own clone's master branch up to date::

    $ git push origin master

* Make a local branch::

    $ git checkout -b my-branch-name

* Make your changes and commit them locally

* Push your changes to **a branch** in your **own** clone at gitlab::

    $ git push origin my-branch-name

* Log in at GitLab_, find the branch, and create a Merge Request.



Changing the PTM module
-----------------------

*This is mostly a 'Note to self'.  I do not expect anybody else to do
this, but need to remember how to do it.*

The PTM submodule could be tracked as a `git submodule`.  However,
while that is nice for the developers, it makes it complicated to
clone the repository for people just wanting to use the developer
version.  Instead, we use 'subtree merging'.

* Be sure the ptm_remote is set up::

    git remote add ptm_remote https://github.com/pmla/polyhedral-template-matching.git

* Fetch from it::

    $ git fetch ptm_remote --no-tags

* **First time only:** Make a branch for the other repo.  Note that this
  branch tracks a different project, so your working copy better be
  clean!

  ::

    $ git checkout -b ptm_tracking ptm_remote/master

* Update from the PTM repo, then switch back to the usual project::

    $ git checkout ptm_tracking
    $ git pull
    $ git checkout master

* Make new branch for the change::

    $ git checkout -b my-new-ptm-change

* Update the PTM code::

    $ git merge --squash -s recursive -X theirs -Xsubtree=PTM ptm_tracking

* Test the update.  Check it in, push it, and make a merge request.



.. _GitLab repository: https://wiki.fysik.dtu.dk/ase/development/contribute.html#gitlab-repository
.. _Making changes: https://wiki.fysik.dtu.dk/ase/development/contribute.html#making-changes
.. _GitLab: https://gitlab.com



