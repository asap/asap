.. _Molybdenum:

======================================
The Molybdenum potential (MoPotential)
======================================

This is a special potential optimized to describe dislocation processes in Molybdenum in the body-centered cubic crystal structure.
