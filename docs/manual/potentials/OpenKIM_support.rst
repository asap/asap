.. _OpenKIM support:

===============
OpenKIM support
===============

The OpenKIM.org_ project aims at sharing potentials between molecular
dynamics codes such as Asap.  The potentials (called "Models" in the
OpenKIM project) can be downloaded and compiled, without any
adaptation to Asap (which acts as a "Simulator" in the OpenKIM sense).

The OpenKIM project is designed to support :ref:`Parallel simulations on clusters` using
the same parallelization model as Asap.  Thus the OpenKIM
Models can directly be used in parallel simulations (barring bugs, of
course).


Prerequisites
=============

The OpenKIM API must be installed and compiled.  Asap must have been
built with OpenKIM support.  This is the case with the
default Niflheim installation.

From version 3.11, Asap uses OpenKIM version 2.X.  This gives simpler memory management, and the issues seen with some version 1 OpenKIM models in parallel simulations are now solved.

Adding new OpenKIM models to Asap
=================================

Newer versions of OpenKIM include a utility for managing models.  You list the
installed models with::

  kim-api-v2-collections-management list

You can add a specific model with the command::

  kim-api-v2-collections-management install user SW_StillingerWeber_1985_Si__MO_405512056662_005

or you can install them all with::

  kim-api-v2-collections-management install user OpenKIM


Using OpenKIM models
====================

An OpenKIM model is used by creating an OpenKIM_Calculator object with
the full KIM model name as the argument.  Example::

  atoms.set_calculator(OpenKIMcalculator('EMT_Asap_Standard_Jacobsen_Stoltze_Norskov_AlAgAuCuNiPdPt__MO_XXXXXXXXXXXX'))

In this case the standard Asap EMT potential is imported as an OpenKIM
model (we are contributing this potential to the OpenKIM project - to appear soonish).  Of
course this is kind of pointless, as using the EMT potential avoids
the overhead of going through the OpenKIM API.  Currently, that
overhead is of the order of XXX% for the EMT potential.

All correctly implemented OpenKIM models can be used with :ref:`Parallel simulations on clusters` using message passing;
but they do **not support** :ref:`Multi-threaded parallelization`.

The OpenKIMcalculator
=====================

``OpenKIMcalculator(name, atoms=None, stress=True, stresses=True, verbose=False)``:
    A calculator interfacing to the OpenKIM models.
        
    ``name``:
	The long name of the KIM Model.
        
    ``atoms``:
	If set, set_atoms is called immediately.  Default: no atoms set.
    
    ``stress``:
	Set to False to refrain from calculate the global virial, even if the model
        supports it.  Default:  True (calculate global virial / stress).
             
    ``stresses``:
	As above, but for atomic virials / stresses.
    
    ``verbose``:
	Set to True to print additional info during neigborlist matching.


Setting model parameters
========================

**XXX To be updated to KIM V2. XXX**


OpenKIM models may have parameters that can be read and modified.  The
interface for doing this is described here.  Both the model's fixed
and free parameters may be accessed, but of course only the free
parameters may be modified.  In the OpenKIM specification, all
parameter names begin with ``PARAM_FREE_`` or ``PARAM_FIXED_``, these prefixes
are removed by this interface.

**WARNING:** Currently, the interface only support floating-point
parameters.  No known OpenKIM models have free parameters of other
types, but the fixed parameters may be integers or even pointers.
Accessing these variables could cause memory access errors
(i.e. crashes).  For this reason, please only access the free
parameters.

``calculator.get_parameters(kind='free')``:
  Get a dictionary with all the parameters.  Per default, only free
  parameters are returned.  If ``kind`` is set to 'fixed' or 'all',
  fixed parameters or all parameters are returned (see warning
  above).  Changing the dictionary or the arrays returned does *not*
  change the parameters as seen by the model.

``calculator.set_parameters(param1=value, ...)``:
  Set one or more free parameters.  The parameters are given as
  keyword arguments with the parameter name as the keyword, and the
  desired value as the value.  Limited type checking is done: a scalar
  parameter can only be set to a floating-point number; a indexed
  parameter can be set to a numpy array of the right size.  Only the
  total number of elements is checked, the detailed shape is not.
  
  After calling this function, the OpenKIM model is automatically
  reinitialized by calling its ``reinit`` method.  Note that if this
  function fails to set one or more parameters (due to incorrect names
  or data types) this reinitialization is not done, and the OpenKIM
  model may be left in an undefined state.  Call this function again
  to fix it.

``calculator.get_fixed_parameter_names()`` and ``calculator.get_fixed_parameter_names()``:
  Low-level access functions.  They return a tuple of parameter
  names.  The ``PARAM_FREE_/PARAM_FIXED_`` prefix is *not* removed.


.. _OpenKIM.org: http://openkim.org
