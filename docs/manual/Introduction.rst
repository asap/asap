.. _Introduction:

Introduction
============

Asap is a tool for doing atomic-scale computer simulations (mainly
molecular dynamics) using classical potentials (mainly Effective
Medium Theory).

Asap is intended to work as an energy calculator in connection with
the Atomic Simulation Environment (ASE_).  However, 
some parts of the ASE_ are either extended or reimplemented for
reasons of efficiency and ease-of-use on parallel computers.  

Do I need Asap?
---------------

If you are installing the ASE software to do Density Functional
Theory calculations with GPAW_, you do *not* need to install Asap.

How does Asap relate to the ASE and to GPAW?
--------------------------------------------

Asap and GPAW_ are both tools for calculating the forces and
energies on atoms.  They both require the ASE_ to be useful, since the
ASE_ contains objects and algorithms necessary to do atomic-scale
simulations.

GPAW_ uses Density Functional Theory, i.e. quantum mechanics, to
calculate the energies.  It is therefore slow but accurate, and can in
principle handle all elements of the periodic table.  Calculations
with GPAW_ are limited to a few hundreds of atoms, or perhaps a few thousand
atoms on a parallel computer.

Asap uses a classical potential, the Effective Medium Theory, to
calculate the energies.  This is an approximation that only works for
some elements (Ni, Cu, Pd, Ag, Pt, Au, Mg), but then it gives
reasonably accurate results very quickly.  With Asap you can do
molecular dynamics of a million atoms on a single PC (if you are
patient), or hundreds of millions on a parallel computer.

How to get started.
-------------------

Once ASE_ and Asap are :ref:`installed <Installation>`, you should probably look at some of
the :ref:`Examples` to get started.



.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _GPAW: http://wiki.fysik.dtu.dk/gpaw/
