#!/bin/sh

mpirun -np 2 python -W error::FutureWarning TestAll.py --parallel "$@"
