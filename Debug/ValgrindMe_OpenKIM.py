from __future__ import print_function
from asap3 import *
from ase.build import bulk


atoms = bulk('Al').repeat((8,8,8))
atoms.set_calculator(OpenKIMcalculator('EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Al__MO_623376124862_001'))

dyn = VelocityVerlet(atoms, 5*units.fs)
dyn.run(2)
print(atoms.get_potential_energy())


