import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from ase.md.nose_hoover_chain import IsotropicMTKNPT
from ase.md.nptberendsen import NPTBerendsen
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution, Stationary
from ase.md import MDLogger
from ase.units import GPa, fs, kB
from ase.build import bulk
import asap3


def make_atoms(T, rng, size):
    "Make atomic system suitable for test."
    atoms = bulk('Cu', cubic=True)
    atoms *= size
    MaxwellBoltzmannDistribution(atoms, temperature_K=T, rng=rng)
    Stationary(atoms)
    atoms.calc = asap3.EMT()
    return atoms

class MeasureStuff:
    "Observer measuring energy, temperature, volume and stress"
    def __init__(self, atoms, const_method=None):
        self.atoms = atoms
        self.const_method = const_method
        self.energies = []
        self.temperatures = []
        self.volumes = []
        self.stresses = []
        self.conserved = []

    def __call__(self):
        e = self.atoms.get_potential_energy() + self.atoms.get_kinetic_energy()
        self.energies.append(e)
        self.temperatures.append(self.atoms.get_temperature())
        self.volumes.append(self.atoms.get_volume())
        self.stresses.append(self.atoms.get_stress(include_ideal_gas=True))
        if self.const_method is not None:
            self.conserved.append(self.const_method())

def exponential(t, A, tau, B):
    "We fit stuff to exponentials"
    return A * np.exp(-t / tau) + B

rng = np.random.default_rng()

size = (2,2,2)
#size = (5,5,5)
T0 = 300
p0 = 2.0 * GPa
dt = 5 * fs
nsteps = 20000
runtime = nsteps * dt
logint = 1000
#taut = runtime / 10    # Energy relaxation time in ideal gas
#taup = runtime / 40
taut = 100 * dt
taup = 1000 * dt
bulkmodulus = 140 * GPa    # Bulk modulus of Cu

atoms = make_atoms(T0 / 10, rng, size)

# Initially use NPTBerendsen to reach desired temperature and pressure
dyn = NPTBerendsen(atoms, timestep=dt, temperature_K=T0, pressure_au=p0, 
                   taut=taut, taup=taup, compressibility_au=1 / bulkmodulus)
log = MDLogger(dyn, atoms, '-', peratom=True, stress=True)
dyn.attach(log, interval=logint)
measure = MeasureStuff(atoms)
dyn.attach(measure, interval=1)
dyn.run(nsteps)

stresses = np.array(measure.stresses)
pressures = -stresses[:,:3].sum(axis=1) / 3 
temperatures = np.array(measure.temperatures)
volumes = np.array(measure.volumes)

# Fit temperature and pressure curves
times = np.arange(len(pressures)) * dt / fs
(DeltaPfit, taup_fit, P_fit), _ = curve_fit(exponential, times, pressures, (-p0, taup, p0))
print('Pressures:', (DeltaPfit, taup_fit, P_fit))
(DeltaTfit, taut_fit, T_fit), _ = curve_fit(exponential, times, temperatures, (-T0, taut, T0))
print('Temperatures:', (DeltaTfit, taut_fit, T_fit))

# Plot initial thermalization
fig, ((ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9)) = plt.subplots(3, 3, figsize=(15,9))
ax1.plot(times, exponential(times, DeltaTfit, taut_fit, T_fit), 'k-')
ax1.plot(times, temperatures, 'g.')
ax1.set_title('Berendsen equilibration: Temperature')
ax1.set_ylabel('Temperature [K]')
ax2.plot(times, pressures / GPa, 'g.')
ax2.plot(times, exponential(times, DeltaPfit, taup_fit, P_fit) / GPa, 'k-')
ax2.set_ylabel('Pressure [GPa]')
ax2.set_title('Berendsen equilibration: Pressure')
ax3.plot(times, volumes, 'g.')
ax3.set_ylabel('Volume [Å$^3$]')
ax3.set_title('Berendsen equilibration: Volume')

# Main run with IsotropicMTKNPT   
dyn = IsotropicMTKNPT(atoms, timestep=dt, temperature_K=T0, pressure_au=p0, 
                      tdamp=taut, pdamp=taup)
log = MDLogger(dyn, atoms, '-', peratom=True, stress=True)
dyn.attach(log, interval=logint)
measure = MeasureStuff(atoms, dyn.get_conserved_energy)
dyn.attach(measure, interval=1)
dyn.run(nsteps * 10)

temperatures2 = np.array(measure.temperatures)
stresses = np.array(measure.stresses)
pressures2 = -stresses[:,:3].sum(axis=1) / 3
times2 = np.arange(len(temperatures2)) * dt / fs
volumes2= np.array(measure.volumes)
energies2 = np.array(measure.energies)
enthalpies2 = energies2 + p0 * volumes2   # External pressure!
conserved_elike2 = np.array(measure.conserved)

# Statistical tests
nstart = len(times2) // 4
stdH = np.std(enthalpies2[nstart:])
stdV = np.std(volumes2[nstart:])
avgV = np.mean(volumes2[nstart:])
avgT = np.mean(temperatures2[nstart:])
avgP = np.mean(pressures2[nstart:])
stdConst = np.std(conserved_elike2)
avgConst = np.mean(conserved_elike2)

# Expected enthalpy fluctuation: sqrt(k_B T^2 3 N k_B) = k_B * T * sqrt(3 * N)
expectedH = kB * T0 * np.sqrt(3 * len(atoms))
expectedV = np.sqrt(kB * T0 * avgV / bulkmodulus)

print()
print(f"Observed enthalpy fluctuation: {stdH:.2f} eV")
print(f"Expected enthalpy fluctuation: {expectedH:.2f} eV")
print(f"Error: {(stdH / expectedH - 1) * 100:.2f}%")
print(f"Average volume {avgV:.2f} Å^3")
print(f"Observed volume fluctuation: {stdV:.2f} Å^3")
print(f"Expected volume fluctuation: {expectedV:.2f} Å^3")
print(f"Error: {(stdV / expectedV - 1) * 100:.2f}%")
print(f"Fluctuations of conserved energy-like quantity: {stdConst} eV (relative: {stdConst/avgConst})")
print()

ax4.plot(times2, volumes2, 'b.')
ax4.set_ylabel('Volume [Å$^3$]')
ax4.set_title('IsotropicMTKNPT: Volume')
ax5.plot(times2, temperatures2, 'b.')
ax5.plot([times2[0], times2[-1]], [T0, T0], 'k:')
ax5.set_ylabel('Temperature [K]')
ax5.set_title('IsotropicMTKNPT: Temperature')
ax6.plot(times2, pressures2 / GPa, 'b.')
ax6.plot([times2[0], times2[-1]], [p0 / GPa, p0 / GPa], 'k:')
ax6.set_ylabel('Pressure [GPa]')
ax6.set_title('IsotropicMTKNPT: Pressure')

n = len(times2) * 49 // 50
ax7.plot(times2[n:], volumes2[n:], 'b.')
ax7.set_ylabel('Volume [Å$^3$]')
ax7.set_xlabel('Time [fs]')
ax7.set_title('IsotropicMTKNPT: Volume (zoom)')
ax8.plot(times2[n:], temperatures2[n:], 'b.')
ax8.plot([times2[n], times2[-1]], [T0, T0], 'k:')
ax8.set_ylabel('Temperature [K]')
ax8.set_xlabel('Time [fs]')
ax8.set_title('IsotropicMTKNPT: Temperature (zoom)')
ax9.plot(times2[n:], pressures2[n:] / GPa, 'b.')
ax9.plot([times2[n], times2[-1]], [p0 / GPa, p0 / GPa], 'k:')
ax9.set_ylabel('Pressure [GPa]')
ax9.set_xlabel('Time [fs]')
ax9.set_title('IsotropicMTKNPT: Pressure (zoom)')

fig.tight_layout()
fig.savefig(f'isotropic_mtk_npt_{size[0]}x{size[1]}x{size[2]}.png', dpi=300)
plt.show(block=True)


