#SBATCH --job-name=verybig_dist
#SBATCH --partition=xeon56
#SBATCH -N 10
#SBATCH -n 560
#SBATCH --time=1:00:00
#SBATCH --output=slurm-%x-%J.out


"""Debug distribution of very big systems."""

import argparse
import platform
import subprocess
import re
import os
import multiprocessing
import time
import json
import numpy as np
from asap3.md.velocitydistribution import MaxwellBoltzmannDistribution, Stationary
from asap3.md.verlet import VelocityVerlet
from asap3.md.langevin import Langevin
from asap3 import EMT
from asap3 import AsapThreads
from asap3.io.bundletrajectory import BundleTrajectory
try:
    from asap3 import MakeParallelAtoms
except ImportError:
    pass  # Will not support MPI if not compiled into code.
from ase.lattice.cubic import FaceCenteredCubic
from ase import units
from ase.parallel import world, parprint, paropen

def main():
    cores = tuple(bestfactors(world.size))
    N1 = 10**7
    for i in range(1, 12):
        N = i * N1
        parprint(f'Loop number {i}')
        ntot = float(N) * world.size / 1e9
        parprint(f'Total number of atoms: {ntot:5.3f} billion atoms.')
        atoms = makesystem(N, True, np.random.default_rng(), cores)

def makesystem(natoms, alloy, rng, mpicores):
    unitcells = natoms // 4
    size = bestfactors(unitcells)
    atoms = FaceCenteredCubic(size=tuple(size), symbol='Ag')
    if alloy:
        gold = rng.random(len(atoms))
        z = atoms.get_atomic_numbers()
        z[gold > 0.5] = 79
        atoms.set_atomic_numbers(z)
    if mpicores:
        # We create a system on each mpi task, then merge them to a big system.
        # No attempt is made to place the atoms on the right mpi task, Asap
        # will have to take care of this when creating the parallel atoms object.
        #
        # Find which block this is, then move atoms there
        x = world.rank % mpicores[0]
        yz = world.rank // mpicores[0]
        y = yz % mpicores[1]
        z = yz // mpicores[1]
        assert(0 <= z < mpicores[2])
        cell = atoms.get_cell()
        # Move atoms
        offset = x * cell[0] + y * cell[1] + z * cell[2]
        cell[0] *= mpicores[0]
        cell[1] *= mpicores[1]
        cell[2] *= mpicores[2]
        atoms.set_cell(cell)
        atoms.positions += offset
        # Connect to a parallel atoms object - will move atoms to correct mpi task.
        distrotime = time.perf_counter()
        atoms = MakeParallelAtoms(atoms, mpicores)
        distrotime = time.perf_counter() - distrotime
        distrotime = world.max(distrotime)
        parprint(f'Distribution time: {distrotime} sec.')    
    return atoms

def bestfactors(n):
    pf = primefactors(n)
    pf.sort()
    pf.reverse()
    factors = np.ones(3, int)
    for f in pf:
        a = np.argmin(factors)
        factors[a] *= f
    return factors

def primefactors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors


if __name__ == '__main__':
    main()
    
